package org.jeecg.system.api.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.system.api.entity.SysDepartRole;

/**
 * @Description: 部门角色
 * @Author: jeecg-boot
 * @Date:   2020-02-12
 * @Version: V1.0
 */
public interface ISysDepartRoleService extends IService<SysDepartRole> {

}
