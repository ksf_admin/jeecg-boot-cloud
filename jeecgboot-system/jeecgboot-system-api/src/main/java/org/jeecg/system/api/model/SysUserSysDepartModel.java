package org.jeecg.system.api.model;

import lombok.Data;
import org.jeecg.system.api.entity.SysDepart;
import org.jeecg.system.api.entity.SysUser;

/**
 * 包含 SysUser 和 SysDepart 的 Model
 *
 * @author sunjianlei
 */
@Data
public class SysUserSysDepartModel {

    private SysUser sysUser;
    private SysDepart sysDepart;

}
