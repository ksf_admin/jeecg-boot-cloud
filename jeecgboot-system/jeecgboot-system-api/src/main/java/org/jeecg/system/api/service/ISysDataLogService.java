package org.jeecg.system.api.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.system.api.entity.SysDataLog;

public interface ISysDataLogService extends IService<SysDataLog> {
	
	/**
	 * 添加数据日志
	 * @param tableName
	 * @param dataId
	 * @param dataContent
	 */
	public void addDataLog(String tableName, String dataId, String dataContent);

}
