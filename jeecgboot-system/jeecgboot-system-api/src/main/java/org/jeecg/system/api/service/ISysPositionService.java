package org.jeecg.system.api.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.system.api.entity.SysPosition;

/**
 * @Description: 职务表
 * @Author: jeecg-boot
 * @Date: 2019-09-19
 * @Version: V1.0
 */
public interface ISysPositionService extends IService<SysPosition> {

}
