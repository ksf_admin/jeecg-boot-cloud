package org.jeecg.system.api.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.system.api.entity.SysFillRule;

/**
 * @Description: 填值规则
 * @Author: jeecg-boot
 * @Date: 2019-11-07
 * @Version: V1.0
 */
public interface ISysFillRuleService extends IService<SysFillRule> {

}
