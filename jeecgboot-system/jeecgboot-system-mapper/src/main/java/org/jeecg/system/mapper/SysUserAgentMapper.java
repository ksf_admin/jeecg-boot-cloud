package org.jeecg.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.system.api.entity.SysUserAgent;

/**
 * @Description: 用户代理人设置
 * @Author: jeecg-boot
 * @Date:  2019-04-17
 * @Version: V1.0
 */
public interface SysUserAgentMapper extends BaseMapper<SysUserAgent> {

}
