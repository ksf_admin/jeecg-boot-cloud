package org.jeecg.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.system.api.entity.SysDepartRolePermission;

/**
 * @Description: 部门角色权限
 * @Author: jeecg-boot
 * @Date:   2020-02-12
 * @Version: V1.0
 */
public interface SysDepartRolePermissionMapper extends BaseMapper<SysDepartRolePermission> {

}
