package org.jeecg.system.biz.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.dubbo.config.annotation.Service;
import org.jeecg.system.api.entity.SysUserAgent;
import org.jeecg.system.api.service.ISysUserAgentService;
import org.jeecg.system.mapper.SysUserAgentMapper;

/**
 * @Description: 用户代理人设置
 * @Author: jeecg-boot
 * @Date:  2019-04-17
 * @Version: V1.0
 */
@Service(version = "${system.service.version}")
public class SysUserAgentServiceImpl extends ServiceImpl<SysUserAgentMapper, SysUserAgent> implements ISysUserAgentService {

}
