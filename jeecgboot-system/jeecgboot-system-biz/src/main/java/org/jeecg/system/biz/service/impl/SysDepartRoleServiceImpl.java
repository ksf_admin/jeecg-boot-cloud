package org.jeecg.system.biz.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.dubbo.config.annotation.Service;
import org.jeecg.system.api.entity.SysDepartRole;
import org.jeecg.system.api.service.ISysDepartRoleService;
import org.jeecg.system.mapper.SysDepartRoleMapper;

/**
 * @Description: 部门角色
 * @Author: jeecg-boot
 * @Date:   2020-02-12
 * @Version: V1.0
 */
@Service(version = "${system.service.version}")
public class SysDepartRoleServiceImpl extends ServiceImpl<SysDepartRoleMapper, SysDepartRole> implements ISysDepartRoleService {

}
