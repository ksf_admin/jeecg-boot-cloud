package org.jeecg.system.biz.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.dubbo.config.annotation.Service;
import org.jeecg.system.api.entity.SysDataSource;
import org.jeecg.system.api.service.ISysDataSourceService;
import org.jeecg.system.mapper.SysDataSourceMapper;

/**
 * @Description: 多数据源管理
 * @Author: jeecg-boot
 * @Date: 2019-12-25
 * @Version: V1.0
 */
@Service(version = "${system.service.version}")
public class SysDataSourceServiceImpl extends ServiceImpl<SysDataSourceMapper, SysDataSource> implements ISysDataSourceService {

}
