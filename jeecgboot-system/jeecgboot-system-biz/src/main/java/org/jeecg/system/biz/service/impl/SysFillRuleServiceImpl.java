package org.jeecg.system.biz.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.dubbo.config.annotation.Service;
import org.jeecg.system.api.entity.SysFillRule;
import org.jeecg.system.api.service.ISysFillRuleService;
import org.jeecg.system.mapper.SysFillRuleMapper;

/**
 * @Description: 填值规则
 * @Author: jeecg-boot
 * @Date: 2019-11-07
 * @Version: V1.0
 */
@Service(version = "${system.service.version}")
public class SysFillRuleServiceImpl extends ServiceImpl<SysFillRuleMapper, SysFillRule> implements ISysFillRuleService {

}
